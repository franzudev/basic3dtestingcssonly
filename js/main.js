window.onload = () => {

    let buttons = [
        document.getElementById('btn1'),
        document.getElementById('btn2'),
        document.getElementById('btn3'),
        document.getElementById('btn4')
    ];

    window.btn = buttons[0];

    let ranges = [
        document.getElementById('perspective'),
        document.getElementById('rotateX'),
        document.getElementById('rotateY'),
        document.getElementById('rotateZ'),
        document.getElementById('translateX'),
        document.getElementById('translateY'),
        document.getElementById('translateZ'),
        document.getElementById('scaleX'),
        document.getElementById('scaleY'),
        document.getElementById('scaleZ')
    ];

    for(let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', focusIt);
    }

    for(let i = 0; i < ranges.length; i++){
        ranges[i].addEventListener('input', changeValue);
    }

};

function focusIt(e) {
    let buttontainer = document.getElementById('buttontainer');
    window.btn = (e.target.id === 'btn4') ? buttontainer : e.target;
}

function changeValue(e) {
    window.btn.style.transform = updateTransform(e.target.id, e.target.value);
}

const updateTransform = (prop, val) => {
    switch(true){
        case prop.includes('rotate'):
            val += 'deg';
            break;
        case prop.includes('scale'):
            break;
        default:
            val += 'px';
            break;
    }

    let str = window.btn.style.transform;
    let newProp = `${prop}(${val})`;
    if (str.indexOf(prop) !== -1) {
        // console.log(str);
        let firstHalf = str.slice(0, str.indexOf(prop));
        let propStart = str.slice(str.indexOf(prop));
        let secondHalf =(propStart.indexOf(')') !== - 1) ? propStart.slice(propStart.indexOf(')') + 1) : propStart;

        newProp = firstHalf + newProp + secondHalf;
    } else {
        newProp = str + ' ' + newProp;
    }

    return newProp;
};
